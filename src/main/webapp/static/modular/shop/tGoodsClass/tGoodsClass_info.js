/**
 * 初始化部门详情对话框
 */
var TArticleInfoDlg = {
    tGoodsClassInfoData : {},
    zTreeInstance : null
};

/**
 * 清除数据
 */
TArticleInfoDlg.clearData = function() {
    this.tGoodsClassInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TArticleInfoDlg.set = function(key, val) {
    this.tGoodsClassInfoData[key] = (typeof value == "undefined") ? $("#" + key).val() : value;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TArticleInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
TArticleInfoDlg.close = function() {
    parent.layer.close(window.parent.TArticle.layerIndex);
}

/**
 * 点击部门ztree列表的选项时
 *
 * @param e
 * @param treeId
 * @param treeNode
 * @returns
 */
TArticleInfoDlg.onClickDept = function(e, treeId, treeNode) {
    $("#pName").attr("value", TArticleInfoDlg.zTreeInstance.getSelectedVal());
    $("#pid").attr("value", treeNode.id);
}

/**
 * 显示部门选择的树
 *
 * @returns
 */
TArticleInfoDlg.showDeptSelectTree = function() {
    var pName = $("#pName");
    var pNameOffset = $("#pName").offset();
    $("#parentDeptMenu").css({
        left : pNameOffset.left + "px",
        top : pNameOffset.top + pName.outerHeight() + "px"
    }).slideDown("fast");

    $("body").bind("mousedown", onBodyDown);
}

/**
 * 隐藏部门选择的树
 */
TArticleInfoDlg.hideDeptSelectTree = function() {
    $("#parentDeptMenu").fadeOut("fast");
    $("body").unbind("mousedown", onBodyDown);// mousedown当鼠标按下就可以触发，不用弹起
}

/**
 * 收集数据
 */
TArticleInfoDlg.collectData = function() {
    this.set('id').set('name').set('title').set('pid');
}

/**
 * 提交添加部门
 */
TArticleInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/tGoodsClass/add", function(data){
        Feng.success("添加成功!");
        window.parent.TArticle.table.refresh();
        TArticleInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.tGoodsClassInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
TArticleInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/tGoodsClass/update", function(data){
        Feng.success("修改成功!");
        window.parent.TArticle.table.refresh();
        TArticleInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.tGoodsClassInfoData);
    ajax.start();
}

function onBodyDown(event) {
    if (!(event.target.id == "menuBtn" || event.target.id == "parentDeptMenu" || $(
            event.target).parents("#parentDeptMenu").length > 0)) {
        TArticleInfoDlg.hideDeptSelectTree();
    }
}

$(function() {
    var ztree = new $ZTree("parentDeptMenuTree", "/tGoodsClass/tree");
    ztree.bindOnClick(TArticleInfoDlg.onClickDept);
    ztree.init();
    TArticleInfoDlg.zTreeInstance = ztree;
});
