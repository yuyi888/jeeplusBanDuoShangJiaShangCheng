/** Powered By 北京zscat科技, Since 2016 - 2020**/
package com.stylefeng.guns.modular.test.controller;

import com.stylefeng.guns.common.annotion.log.BussinessLog;
import com.stylefeng.guns.common.constant.Dict;
import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.controller.BaseController;
import com.stylefeng.guns.common.exception.BizExceptionEnum;
import com.stylefeng.guns.common.exception.BussinessException;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.shiro.ShiroKit;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.test.service.ITCardService;
import com.stylefeng.guns.persistence.test.model.TCard;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.stylefeng.guns.modular.system.warpper.DeptWarpper;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 确认管理控制器
 * [email]
 * @author zscat 951449465
 * @Date 2017-5-22 13:34:32
 */
@Controller
@RequestMapping("/tCard")
public class TCardController extends BaseController {

    private String PREFIX = "/test/tCard/";

    @Resource
    private ITCardService TCardService;

    /**
     * 跳转到确认管理列表首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "tCard.html";
    }

    /**
     * 跳转到添加确认管理
     */
    @RequestMapping("/tCard_add")
    public String tCardAdd() {
        return PREFIX + "tCard_add.html";
    }

    /**
     * 跳转到修改确认管理
     */
    @RequestMapping("/tCard_update/{tCardId}")
    public String tCardUpdate(@PathVariable Integer tCardId, Model model) {
        TCard tCard = TCardService.selectById(tCardId);
        model.addAttribute("tCard",tCard);
        return PREFIX + "tCard_edit.html";
    }



    /**
     * 获取确认管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
          List<Map<String, Object>> list = TCardService.selectMaps(null);
         return super.warpObject(new DeptWarpper(list));
    }

    /**
     * 新增确认管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(TCard tCard) {
        TCardService.insert(tCard);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除确认管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Long id) {
        TCardService.deleteById(id);
        return SUCCESS_TIP;
    }

    /**
     * 修改确认管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(TCard tCard) {
        TCardService.updateById(tCard);
        return super.SUCCESS_TIP;
    }

}
