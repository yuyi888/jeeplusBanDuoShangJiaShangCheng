package com.stylefeng.guns.modular.shop.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.persistence.shop.model.TCart;
import com.stylefeng.guns.persistence.shop.dao.TCartMapper;
import com.stylefeng.guns.modular.shop.service.ITCartService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@Service
public class TCartServiceImpl extends ServiceImpl<TCartMapper, TCart> implements ITCartService {

    @Override
    public int selectOwnCartCount(Long id) {
        TCart cart = new TCart();
        cart.setUserid(id);
        return this.selectCount(new EntityWrapper<>(cart));
    }

    @Override
    public List<TCart> selectOwnCart(Long id) {
        TCart cart = new TCart();
        cart.setUserid(id);
        return this.selectList(new EntityWrapper<>(cart));
    }
}
