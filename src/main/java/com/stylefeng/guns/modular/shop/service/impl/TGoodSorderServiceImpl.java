package com.stylefeng.guns.modular.shop.service.impl;

import com.stylefeng.guns.persistence.shop.model.TGoodSorder;
import com.stylefeng.guns.persistence.shop.dao.TGoodSorderMapper;
import com.stylefeng.guns.modular.shop.service.ITGoodSorderService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@Service
public class TGoodSorderServiceImpl extends ServiceImpl<TGoodSorderMapper, TGoodSorder> implements ITGoodSorderService {
	
}
