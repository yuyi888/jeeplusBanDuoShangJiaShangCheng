package com.stylefeng.guns.modular.shop.controller;

import com.stylefeng.guns.common.annotion.Permission;
import com.stylefeng.guns.common.annotion.log.BussinessLog;
import com.stylefeng.guns.common.constant.Const;
import com.stylefeng.guns.common.constant.Dict;
import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.controller.BaseController;
import com.stylefeng.guns.common.exception.BizExceptionEnum;
import com.stylefeng.guns.common.exception.BussinessException;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.shop.service.ITArticleService;
import com.stylefeng.guns.modular.system.warpper.DeptWarpper;
import com.stylefeng.guns.persistence.shop.model.TArticle;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@Controller
@RequestMapping("/tArticle")
public class TArticleController extends BaseController {
    private String PREFIX = "/shop/tArticle/";

    @Resource
    ITArticleService itLinkService;


    /**
     * 跳转到部门管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "tArticle.html";
    }

    /**
     * 跳转到添加部门
     */
    @RequestMapping("/tArticle_add")
    public String tArticleAdd() {
        return PREFIX + "tArticle_add.html";
    }

    /**
     * 跳转到修改部门
     */
    @RequestMapping("/tArticle_update/{tArticleId}")
    public String tArticleUpdate(@PathVariable Integer tArticleId, Model model) {
        TArticle tArticle = itLinkService.selectById(tArticleId);
        model.addAttribute("tArticle",tArticle);
        LogObjectHolder.me().set(tArticle);
        return PREFIX + "tArticle_edit.html";
    }


    /**
     * 新增部门
     */
    @BussinessLog(value = "添加部门", key = "simplename", dict = Dict.DeptDict)
    @RequestMapping(value = "/add")
    @ResponseBody
    @Permission(Const.ADMIN_NAME)
    public Object add(TArticle tArticle) {
        return this.itLinkService.insert(tArticle);
    }

    /**
     * 获取所有部门列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        List<Map<String, Object>> list = this.itLinkService.selectMaps(null);
        return super.warpObject(new DeptWarpper(list));
    }

    /**
     * 部门详情
     */
    @RequestMapping(value = "/detail/{tArticleId}")
    @ResponseBody
    public Object detail(@PathVariable("tArticleId") Integer tArticleId) {
        return itLinkService.selectById(tArticleId);
    }

    /**
     * 修改部门
     */
    @BussinessLog(value = "修改部门", key = "simplename", dict = Dict.DeptDict)
    @RequestMapping(value = "/update")
    @ResponseBody
    @Permission(Const.ADMIN_NAME)
    public Object update(TArticle tArticle) {
        if (ToolUtil.isEmpty(tArticle) || tArticle.getId() == null) {
            throw new BussinessException(BizExceptionEnum.REQUEST_NULL);
        }
        itLinkService.updateById(tArticle);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除部门
     */
    @BussinessLog(value = "删除部门", key = "tArticleId", dict = Dict.DeleteDict)
    @RequestMapping(value = "/delete")
    @ResponseBody
    @Permission(Const.ADMIN_NAME)
    public Object delete(@RequestParam Long tArticleId) {

        itLinkService.deleteById(tArticleId);

        return SUCCESS_TIP;
    }
}
