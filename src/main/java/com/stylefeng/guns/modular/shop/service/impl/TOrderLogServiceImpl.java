package com.stylefeng.guns.modular.shop.service.impl;

import com.stylefeng.guns.persistence.shop.model.TOrderLog;
import com.stylefeng.guns.persistence.shop.dao.TOrderLogMapper;
import com.stylefeng.guns.modular.shop.service.ITOrderLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单处理历史表 服务实现类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@Service
public class TOrderLogServiceImpl extends ServiceImpl<TOrderLogMapper, TOrderLog> implements ITOrderLogService {
	
}
